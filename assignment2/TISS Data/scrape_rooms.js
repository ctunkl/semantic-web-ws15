function appendHeader(obj) {
	$("#tableForm\\:roomTbl_head th").each(function(i) {
		obj.text += $(this).text() + "\t";
	});
}

function appendData(obj) {
    var nextButton = $("#tableForm\\:roomTbl_paginator_bottom .ui-paginator-next");

	if(nextButton.hasClass("ui-state-disabled")) {
		appendTableData(obj);
		console.log("finished");
	} else {
		appendTableData(obj);
		$(nextButton).trigger("click");

		setTimeout(function() {
			appendData(obj);
		}, 200);
	}
}

function appendTableData(obj) {
	$("#tableForm\\:roomTbl_data tr").each(function(i) {
		obj.text += "\n";
		$(this).find("td").each(function(j) {
			obj.text += $(this).text() + "\t";
		});
	});
}


var content = { text: "" };

appendHeader(content);
appendData(content);

// Print content.text when finished