var projects = {};
var $tables = $('.projects');
var tablesIndex = -1;

function fetchTable() {
	tablesIndex++;

	if(tablesIndex < $tables.length) {
		fetchTableContent($($tables[tablesIndex]));
	}
	else {
		fetchProjectMembers();
	}
}

function fetchTableContent($table) {
	$table.find('table[role="grid"] tbody tr').each(function() {
		var $tds = $(this).find('td');

		var id = $($tds[0]).find('a').attr('href').substring(28);
		if(!projects.hasOwnProperty(id)) {
			projects[id] = {
				id: id,
				link: $($tds[0]).find('a').attr('href'),
				title: $($tds[0]).text().trim(),
				begin: $($tds[1]).text().trim(),
				end: $($tds[2]).text().trim(),
				institute: $($tds[3]).text().trim(),
				members: []
			};
		};
	});

	var $next = $table.find('.ui-paginator-next:not(.ui-state-disabled)');
	if($next.length) {
		$next.click();
		window.setTimeout(fetchTableContent.bind(this, $table), 1000);
	}
	else {
		fetchTable();
	}
}

function fetchProjectMembers() {
	var ids = Object.keys(projects);

	for(var i = 0; i < ids.length; i++) {
		var id = ids[i];
		$.ajax({
			url: 'https://tiss.tuwien.ac.at/fpl/project/index.xhtml?id=' + id,
			dataType: 'html'
		}).done(function(id, data) {
			var $persons = $(data).find('a[href^="https://tiss.tuwien.ac.at/adressbuch/adressbuch/person_via_oid/"]');
			$persons.each(function() {
				projects[id].members.push($(this).attr('href').substring(63));
			});
		}.bind(this, id));
	}

	$(document).ajaxStop(function() {
		console.log(object2text());
	});
}

function object2text() {
	var output = 'Id\tLink\tTitle\tBegin\tEnd\tInstitute\tMembers\n';

	var ids = Object.keys(projects);

	for(var i = 0; i < ids.length; i++) {
		var id = ids[i];
		
		output+= projects[id].id + '\t';
		output+= projects[id].link + '\t';
		output+= projects[id].title + '\t';
		output+= projects[id].begin + '\t';
		output+= projects[id].end + '\t';
		output+= projects[id].institute + '\t';
		output+= projects[id].members.join(',') + '\t';
		output += '\n';
	}

	return output;
}

// fetchTable();