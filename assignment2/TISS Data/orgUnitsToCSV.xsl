<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tu="https://tiss.tuwien.ac.at/api/schemas/orgeinheit/v14">
	<xsl:output method="text" encoding="utf-8" />

	<xsl:strip-space elements="*" indent="no"/>

  	<xsl:template match="/">
		<xsl:text>oid;type;orgUnitNumber;name;mbox;phone;parentUnitOID</xsl:text>
  		<xsl:text>
</xsl:text>
  		<xsl:apply-templates />
  	</xsl:template>

	<xsl:template match="tu:orgunit">
			<xsl:value-of select="@oid"/><xsl:text>;</xsl:text>
			<xsl:value-of select="tu:type"/><xsl:text>;</xsl:text>
			<xsl:value-of select="tu:orgunit_number"/><xsl:text>;</xsl:text>
			<xsl:value-of select="tu:name/tu:de"/><xsl:text>;</xsl:text>
			<xsl:value-of select="tu:emails/*[1]"/><xsl:text>;</xsl:text>
			<xsl:value-of select="tu:phone_numbers/*[1]"/><xsl:text>;</xsl:text>
			<xsl:value-of select="tu:parent_ref/@oid"/>
	</xsl:template>
</xsl:stylesheet>