var data = {
	faculties: [],
	orgunits: [],
	personsIds: [],
	persons: [],
};

var $ = jQuery;

function fetchFaculties() {
	console.log('fetching faculties ...');

	$('a[href^="/adressbuch/adressbuch/fakultaet/"]').each(function() {
		data.faculties.push($(this).attr('href'));
	});

	fetchInstitutes();
};

function fetchInstitutes() {
	console.log('fetching institutes ...');

	data.faculties = $.unique(data.faculties);

	for(var i = 0; i < data.faculties.length; i++) {
		$.ajax({
			url: data.faculties[i],
			dataType: 'html'
		}).done(function(d) {
			$(d).find('ul li ul li a[href^="/adressbuch/adressbuch/orgeinheit/"]:not(.supNavMenuHeader)').each(function() {
				data.orgunits.push($(this).attr('href'));
			});

			$(d).find('a[href^="/adressbuch/adressbuch/person/"]').each(function() {
				data.personsIds.push($(this).attr('href').substring(30));
			});
		});
	}

	$(document).ajaxStop(function() {
		$(this).unbind('ajaxStop');
		fetchOrgunit();
	});
};

function fetchOrgunit() {
	console.log('fetching orgunits ...');

	data.orgunits = $.unique(data.orgunits);

	for(var i = 0; i < data.orgunits.length; i++) {
		$.ajax({
			url: data.orgunits[i],
			dataType: 'html'
		}).done(function(d) {
			$(d).find('a[href^="/adressbuch/adressbuch/person/"]').each(function() {
				data.personsIds.push($(this).attr('href').substring(30));
			});
		});
	}

	$(document).ajaxStop(function() {
		$(this).unbind('ajaxStop');
		fetchPersons();
	});
}

function fetchPersons() {
	console.log('fetching persons ...');

	data.personsIds = $.unique(data.personsIds);

	for(var i = 0; i < data.personsIds.length; i++) {
		$.ajax({
			url: '/adressbuch/adressbuch/person/' + data.personsIds[i] + '.xml',
			dataType: 'xml'
		}).done(function(id, xml) {
			var $xml = $(xml);

			var person = {
				oid: $xml.find('person').attr('oid'),
				tissid: id,
				firstname: $xml.find('firstname').text(),
				lastname: $xml.find('lastname').text(),
				gender: $xml.find('gender').text(),
				mbox: $($xml.find('emails').children().get(0)).text(),
				phone: $($xml.find('phone_numbers').children().get(0)).text(),
				rooms: [],
				employments: []
			};

			$xml.find('room_code').each(function() { person.rooms.push($(this).text()); });
			$xml.find('employment organisational_unit').each(function() { person.employments.push($(this).attr('oid')); });

			data.persons.push(person);

		}.bind(this, data.personsIds[i]));
	}

	$(document).ajaxStop(function() {
		console.log(object2text());
	});
}

function object2text() {
	var output = 'Oid\tFirstname\tLastname\tgender\tmbox\tphone\trooms\temployments\n';

	for(var i = 0; i < data.persons.length; i++) {		
		output+= data.persons[i].oid + '\t';
		output+= data.persons[i].firstname + '\t';
		output+= data.persons[i].lastname + '\t';
		output+= data.persons[i].gender + '\t';
		output+= data.persons[i].mbox + '\t';
		output+= data.persons[i].phone + '\t';
		output+= data.persons[i].rooms.join(',') + '\t';
		output+= data.persons[i].employments.join(',') + '\t';
		output += '\n';
	}

	return output;
}