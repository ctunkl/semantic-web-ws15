#!/bin/bash

output=orgunits.csv

for f in orgunits/orgunit*.xml; do
	line=$(xml tr orgUnitsToCSV.xsl $f)

	# Strip header if file already exists
	if [ -f $output ]; then
		 line=$(echo "$line" | tail -n +2)
	fi

	echo "$line" | tee -a $output
done



