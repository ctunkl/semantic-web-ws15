#!/bin/bash

fetchOrgUnits() {
	# Read all oids, including parents, children and self
	xml sel -t -v  "//*/@oid" -n $1 | while read -r oid ; do
		filename="orgunit_$oid.xml"
		file=orgunits/$filename

		# Download data if necessary
		if [ ! -f $file ]; then
			echo "Downloading orgunit $oid..."
			curl -s "https://tiss.tuwien.ac.at/api/org_unit/oid/$oid" >$file
		fi
	done
}


mkdir -p orgunits

for f in faculties/faculty_*.xml; do
	fetchOrgUnits $f
done



oldFileCount=$(ls -l | wc -l)
for f in orgunits/orgunit_*.xml; do
	fetchOrgUnits $f
done
fileCount=$(ls -l | wc -l)

# Keep fetching until we've got all orgunits
while [ $fileCount -ne $oldFileCount ]; do
	oldFileCount=$(ls -l | wc -l)
	for f in orgunits/orgunit_*.xml; do
		fetchOrgUnits $f
	done
	fileCount=$(ls -l | wc -l)
done