var courses = [];

function addCourseIds() {
	$('a[href^="/course/courseDetails.xhtml"]').each(function() {
		var href = $(this).attr('href');
		var data = href.match(/courseNr=([a-z0-9]*)&semester=([a-z0-9]*)/i);
		if(data != null) {
			courses.push(data[1] + '-' + data[2]);
		}
	});
}

addCourseIds();

var courseData = [];

function fetchCourse() {
	if(courses.length == 0) {
		console.log(object2text());
		return;
	}

	var courseId = courses.shift();

	$.ajax({
		url: 'https://tiss.tuwien.ac.at/api/course/' + courseId,
		dataType: 'xml'
	}).done(function(xml) {
		var $xml = $(xml);
		var cd = {
			number: $xml.find('courseNumber').text(),
			term: $xml.find('semesterCode').text(),
			type: $xml.find('courseType').text(),
			title: $($xml.find('title').children().eq(0)).text(),
			institute: $xml.find('instituteCode').text(),
			lecturers: [],
			weeklyHours: parseFloat($xml.find('weeklyHours').text()),
			ects: 0,
		};

		cd.ects = cd.weeklyHours * 1.5;
		$xml.find('lecturers oid').each(function() { cd.lecturers.push($(this).text()); });

		courseData.push(cd);

		fetchCourse();
	}).fail(function(e) {
		console.log(e);
		fetchCourse();
	});
}

function object2text() {
	var output = 'Number\tTerm\tType\tTitle\tInstitute\tLectuerers\tWeekyHours\tects\n';

	for(var i = 0; i < courseData.length; i++) {
		
		output += courseData[i].number + '\t';
		output += courseData[i].term + '\t';
		output += courseData[i].type + '\t';
		output += courseData[i].title + '\t';
		output += courseData[i].institute + '\t';
		output += courseData[i].lecturers.join(',') + '\t';
		output += courseData[i].weeklyHours + '\t';
		output += courseData[i].ects + '\t';
		output += '\n';
	}

	return output;
}

fetchCourse();