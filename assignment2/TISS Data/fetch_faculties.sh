#!/bin/bash

mkdir -p faculties

tail -n +2 faculties.csv | while IFS=, read -r name url tissid ; do
	curl -s "$url.xml" >"faculties/faculty_$tissid.xml"
done