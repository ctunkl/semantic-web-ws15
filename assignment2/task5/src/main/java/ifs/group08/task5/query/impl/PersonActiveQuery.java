package ifs.group08.task5.query.impl;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import ifs.group08.task5.query.AbstractQuery;
import ifs.group08.task5.query.QueryParameter;

public class PersonActiveQuery extends AbstractQuery {
	
	public PersonActiveQuery() {
		super();
		
		this.parameters.put("limit", new QueryParameter("limit", "the number of persons returned", false, true));
	}

	@Override
	public String getKey() {
		return "person-active";
	}

	@Override
	public String getDescription() {
		return "get a list of the most active persons, who held courses";
	}

	@Override
	public boolean execute() {
		int limit = this.parameters.get("limit").getValue() == null ? 10 : Integer.parseInt(this.parameters.get("limit").getValue());

		String query = "PREFIX ifs: <http://ifs.tuwien.ac.at/tulid/group08/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
				"SELECT ?firstName ?lastName (COUNT(?course) as ?courseCount) " +
				"WHERE { " +
				"  ?person a ifs:Lecturer; " +
				"	foaf:firstName ?firstName; " +
				"	foaf:lastName ?lastName. " +
				"  ?course a ifs:Course; " +
				"    ifs:courseHeldBy ?person; " +
				"} " +
				"GROUP BY ?firstName ?lastName " +
				"ORDER BY DESC(?courseCount) " +
				"LIMIT " + limit;
		
		ResultSet results = this.sparqlService.executeQuery(query);
		
		System.out.println(limit + " most active persons:\n");
		
		QuerySolution qs;
		int i = 1;
		while(results.hasNext()) {
			qs = results.next();
			System.out.println(
				String.format(
					"%s: %s courses held by %s %s",
					i++,
					qs.getLiteral("courseCount").getInt(),
					qs.getLiteral("lastName"),
					qs.getLiteral("firstName")
				)
			);
		}
		
		return true;
	}

}
