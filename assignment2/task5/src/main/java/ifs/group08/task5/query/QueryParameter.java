package ifs.group08.task5.query;

public class QueryParameter {

	private String key;
	private String description;
	private boolean isRequired;
	private boolean hasArgument;
	private String value;
	
	public QueryParameter(String key, String description) {
		this.key = key;
		this.description = description;
		this.isRequired = false;
		this.hasArgument = false;
		this.value = null;
	}
	
	public QueryParameter(String key, String description, boolean isRequired) {
		this(key, description);
		this.isRequired = isRequired;
	}
	
	public QueryParameter(String key, String description, boolean isRequired, boolean hasArgument) {
		this(key, description, isRequired);
		this.hasArgument = hasArgument;
	}
	
	public String getKey() {
		return this.key;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public boolean isRequired() {
		return this.isRequired;
	}
	
	public boolean hasArgument() {
		return this.hasArgument;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
}
