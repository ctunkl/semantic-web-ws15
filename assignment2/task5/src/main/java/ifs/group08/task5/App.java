package ifs.group08.task5;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;

import ifs.group08.task5.query.QueryController;
import ifs.group08.task5.query.impl.BuildingsQuery;
import ifs.group08.task5.query.impl.InstituteActiveQuery;
import ifs.group08.task5.query.impl.OrgUnitActiveQuery;
import ifs.group08.task5.query.impl.PersonActiveQuery;
import ifs.group08.task5.query.impl.PersonSearchQuery;
import ifs.group08.task5.service.SparqlService;

public class App {

	public static void main(String[] args) {
		
		// create the command line parser
		CommandLineParser parser = new DefaultParser();

		// create the Options
		Options options = new Options();
		options.addOption(Option
			.builder("h")
			.longOpt("help")
			.desc("print this screen")
			.build()
		);
		options.addOption(Option
				.builder("e")
				.longOpt("endpoint")
				.desc("url of the sparql endpoint")
				.required()
				.hasArg()
				.build()
			);
		options.addOption(Option
			.builder("q")
			.longOpt("query")
			.desc("execute a specified query")
			.required()
			.hasArg()
			.build()
		);

		try {
		    CommandLine commandLine = parser.parse(options, args, true);
		    
		    if(!commandLine.hasOption("query")) {
		    	(new HelpFormatter()).printHelp("task5.sh [options]", "options:", options, null);
		    	return;
		    }
		   
		    String queryKey = commandLine.getOptionValue("query");
		    QueryController queryController = new QueryController();
		    
		    queryController.registerQuery(new PersonSearchQuery());
		    queryController.registerQuery(new PersonActiveQuery());
		    queryController.registerQuery(new BuildingsQuery());
		    queryController.registerQuery(new InstituteActiveQuery());
		    queryController.registerQuery(new OrgUnitActiveQuery());
		    
		    if(!queryController.queryExists(queryKey)) {
		    	System.out.println("The choosen query does not exists.");
		    	System.out.println("Valid options:");
		    	System.out.println(" - " + StringUtils.join(queryController.getQueryKeys(), "\n - "));
		    	return;
		    }
		    
	    	options = queryController.getQueryOptions(queryKey);
		    if(commandLine.hasOption("help")) {
		    	(new HelpFormatter()).printHelp("task5.sh --query " + queryKey + " [options]", queryController.getQueryDescription(queryKey) + "\n\noptions:", options, null);
		    	return;
		    }
		    
		    SparqlService.getInstance().setEndpoint(commandLine.getOptionValue("endpoint"));
		    
		    commandLine = parser.parse(options, commandLine.getArgs(), true);
		    queryController.dispatch(queryKey, commandLine);
		    
		} catch(ParseException e) {
			System.out.println(e.getMessage());
			System.out.println("");
			(new HelpFormatter()).printHelp("task5.sh [options]", "options:", options, null);
		}
		catch(Exception exp) {
		    System.out.println(exp.getMessage());
		}
    }
}
