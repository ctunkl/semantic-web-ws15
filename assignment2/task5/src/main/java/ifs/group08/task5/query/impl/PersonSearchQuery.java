package ifs.group08.task5.query.impl;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import ifs.group08.task5.query.AbstractQuery;
import ifs.group08.task5.query.QueryParameter;

public class PersonSearchQuery extends AbstractQuery {
	
	public PersonSearchQuery() {
		super();
		this.parameters.put("sword", new QueryParameter("sword", "search word", true, true));
	}

	@Override
	public String getKey() {
		return "person-search";
	}

	@Override
	public String getDescription() {
		return "search for persons matching given criteria";
	}

	@Override
	public boolean execute() {
		String sword = this.parameters.get("sword").getValue();
		String query = "PREFIX ifs: <http://ifs.tuwien.ac.at/tulid/group08/> " +
			"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
			"SELECT ?firstName ?lastName ?gender ?mbox ?phone " +
			"WHERE { " +
			"  ?person a ifs:Person; " +
			"    foaf:firstName ?firstName; " +
			"    foaf:lastName ?lastName; " +
			"    foaf:gender ?gender; " +
			"    foaf:mbox ?mbox; " +
			"    foaf:phone ?phone. " +
			"    FILTER(regex(?firstName, \".*" + sword + ".*\", \"i\") || regex(?lastName, \".*" + sword + ".*\", \"i\")). " +
			"} ";
		
		ResultSet results = this.sparqlService.executeQuery(query);
		
		QuerySolution qs;
		int i = 0;
		while(results.hasNext()) {
			qs = results.next();
			System.out.println(
				String.format(
					"%s %s [%s] <%s> <%s>",
					qs.getLiteral("lastName"),
					qs.getLiteral("firstName"),
					qs.getLiteral("gender"),
					qs.getResource("mbox"),
					qs.getResource("phone")
				)
			);
			i++;
		}
		
		System.out.println("\n" + i + " persons found for search word '" + sword + "'.");
		
		return true;
	}

}
