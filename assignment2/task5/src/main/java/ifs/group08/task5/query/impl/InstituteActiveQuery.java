package ifs.group08.task5.query.impl;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import ifs.group08.task5.query.AbstractQuery;
import ifs.group08.task5.query.QueryParameter;

public class InstituteActiveQuery extends AbstractQuery {
	
	public InstituteActiveQuery() {
		super();

		this.parameters.put("limit", new QueryParameter("limit", "the number of buildings returned", false, true));
		this.parameters.put("date", new QueryParameter("date", "a specific date where a project has to be 'active' (format: Y-m-d)", false, true));
	}

	@Override
	public String getKey() {
		return "institute-active";
	}

	@Override
	public String getDescription() {
		return "get a list of institutes, which handle the most projects";
	}

	@Override
	public boolean execute() {
		int limit = this.parameters.get("limit").getValue() == null ? 10 : Integer.parseInt(this.parameters.get("limit").getValue());
		String date = this.parameters.get("date").getValue();

		String query = "PREFIX ifs: <http://ifs.tuwien.ac.at/tulid/group08/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> " +
				"SELECT ?orgUnitNumber ?name (COUNT(?project) as ?projectCount) " +
				"WHERE { " +
				"  ?orgUnit a ifs:Institute; " +
				"	ifs:orgUnitNumber ?orgUnitNumber; " +
				"	foaf:name ?name. " +
				"  ?project a ifs:Project; " +
				"    ifs:projectBelongsTo ?orgUnit; " +
				"    ifs:beginDate ?beginDate; " +
				"    ifs:end ?endDate. " +
				(date != null ? "  FILTER(?endDate > \"" + date + "\"^^xsd:date && \"" + date + "\"^^xsd:date > ?beginDate) " : "") +
				"} " +
				"GROUP BY ?orgUnitNumber ?name " +
				"ORDER BY DESC(?projectCount) " +
				"LIMIT " + limit;
		
		ResultSet results = this.sparqlService.executeQuery(query);
		
		System.out.println(limit + " most active institutes" + (date != null ? " on date " + date : "") + ":\n");
		
		QuerySolution qs;
		int i = 1;
		while(results.hasNext()) {
			qs = results.next();
			System.out.println(
				String.format(
					"%s: %s projects from institute %s [%s]",
					i++,
					qs.getLiteral("projectCount").getInt(),
					qs.getLiteral("name"),
					qs.getLiteral("orgUnitNumber")
				)
			);
		}
		
		return true;
	}

}
