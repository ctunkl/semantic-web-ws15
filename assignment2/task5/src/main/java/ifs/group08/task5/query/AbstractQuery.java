package ifs.group08.task5.query;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ifs.group08.task5.service.SparqlService;

public abstract class AbstractQuery {

	protected SparqlService sparqlService;
	protected Map<String, QueryParameter> parameters;
	
	public AbstractQuery() {
		this.sparqlService = SparqlService.getInstance();
		this.parameters = new HashMap<String, QueryParameter>();
	}
	
	public Set<QueryParameter> getParameters() {
		return new HashSet<QueryParameter>(parameters.values());
	}

	public abstract String getKey();
	public abstract String getDescription();
	public abstract boolean execute();
	
}
