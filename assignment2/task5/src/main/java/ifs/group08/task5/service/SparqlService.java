package ifs.group08.task5.service;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;

public class SparqlService {
	private static SparqlService INSTANCE = null;

	private String endpoint;

	private SparqlService() {}

	public static SparqlService getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new SparqlService();
		}
		
		return INSTANCE;
	}
	
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
	public ResultSet executeQuery(String query) {
		QueryExecution queryEx = QueryExecutionFactory.sparqlService(this.endpoint, query);
		return queryEx.execSelect();
	}
}
