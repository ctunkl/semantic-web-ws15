package ifs.group08.task5.query.impl;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import ifs.group08.task5.query.AbstractQuery;
import ifs.group08.task5.query.QueryParameter;

public class OrgUnitActiveQuery extends AbstractQuery {
	
	public OrgUnitActiveQuery() {
		super();
		
		this.parameters.put("limit", new QueryParameter("limit", "the number of orgunits returned", false, true));
		this.parameters.put("coursecount", new QueryParameter("coursecount", "the number of courses the persons have to hold", true, true));
	}

	@Override
	public String getKey() {
		return "orgunit-active";
	}

	@Override
	public String getDescription() {
		return "get a list of institutes, which employ the most persons, who hold at least a specified number of courses";
	}

	@Override
	public boolean execute() {
		int limit = this.parameters.get("limit").getValue() == null ? 10 : Integer.parseInt(this.parameters.get("limit").getValue());
		int coursecount = Integer.parseInt(this.parameters.get("coursecount").getValue());

		String query = "PREFIX ifs: <http://ifs.tuwien.ac.at/tulid/group08/> " + 
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " + 
				"SELECT ?orgUnitNumber ?name (COUNT(?person) as ?personCount) " + 
				"WHERE { " + 
				"  { ?orgUnit a ifs:Institute. } " + 
				"  UNION { ?orgUnit a ifs:Deanery. } " + 
				"  UNION { ?orgUnit a ifs:University. } " + 
				"  UNION { ?orgUnit a ifs:Organization_Unit. } " + 
				"  ?orgUnit ifs:orgUnitNumber ?orgUnitNumber; " + 
				"    foaf:name ?name. " + 
				"  ?person ifs:employedBy ?orgUnit. " + 
				"  { " + 
				"  	 SELECT ?person (COUNT(?course) as ?courseCount) " + 
				"    WHERE { " + 
				"      ?person a ifs:Person, " + 
				"            ifs:Lecturer. " + 
				"      ?course a ifs:Course; " + 
				"        ifs:courseHeldBy ?person " + 
				"    } " + 
				"    GROUP BY ?person " + 
				"    HAVING (?courseCount >= " + coursecount + ") " + 
				"  } " + 
				"} " + 
				"GROUP BY ?orgUnitNumber ?name " + 
				"ORDER BY DESC(?personCount) " + 
				"LIMIT " + limit;
		
		ResultSet results = this.sparqlService.executeQuery(query);
		
		System.out.println(limit + " most active org units (with persons who hold " + coursecount + " or more courses:\n");
		
		QuerySolution qs;
		int i = 1;
		while(results.hasNext()) {
			qs = results.next();
			System.out.println(
				String.format(
					"%s: %s persons in %s [%s]",
					i++,
					qs.getLiteral("personCount").getInt(),
					qs.getLiteral("name"),
					qs.getLiteral("orgUnitNumber")
				)
			);
		}
		
		return true;
	}

}
