package ifs.group08.task5.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Option.Builder;
import org.apache.commons.cli.Options;

public class QueryController {

	private Map<String, AbstractQuery> queries;
	
	public QueryController() {
		this.queries = new HashMap<String, AbstractQuery>();
	}
	
	public void registerQuery(AbstractQuery query) {
		this.queries.put(query.getKey(), query);
	}
	
	public Set<String> getQueryKeys() {
		return this.queries.keySet();
	}
	
	public boolean queryExists(String key) {
		return this.queries.keySet().contains(key);
	}
	
	public AbstractQuery getQuery(String key) {
		if(!this.queryExists(key)) return null;
		return this.queries.get(key);
	}
	
	public String getQueryDescription(String key) {
		if(!this.queryExists(key)) return null;
		return this.queries.get(key).getDescription();
	}
	
	public Options getQueryOptions(String key) {
		if(!this.queryExists(key)) return null;

		Options o = new Options();
		
		AbstractQuery query = this.queries.get(key);
		Set<QueryParameter> parameters = query.getParameters();
		
		Iterator<QueryParameter> i = parameters.iterator();
		while(i.hasNext()) {
			QueryParameter parameter = i.next();
			
			Builder b = Option.builder();
			b.longOpt(parameter.getKey());
			b.desc(parameter.getDescription() + (parameter.isRequired() ? " [required]" : ""));
			if(parameter.isRequired()) b.required();
			if(parameter.hasArgument()) b.hasArg();
			
			o.addOption(b.build());
		}
		
		return o;
	}
	
	public boolean dispatch(String key, CommandLine commandLine) {
		AbstractQuery query = this.queries.get(key);
		Set<QueryParameter> parameters = query.getParameters();
		
		Iterator<QueryParameter> i = parameters.iterator();
		while(i.hasNext()) {
			QueryParameter parameter = i.next();
			
			if(commandLine.hasOption(parameter.getKey())) {
				if(parameter.hasArgument()) {
					parameter.setValue(commandLine.getOptionValue(parameter.getKey()));
				}
				else {
					parameter.setValue("true");
				}
			}
		}
		
		return query.execute();
	}
	
}
