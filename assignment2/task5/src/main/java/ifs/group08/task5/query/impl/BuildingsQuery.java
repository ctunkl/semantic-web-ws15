package ifs.group08.task5.query.impl;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import ifs.group08.task5.query.AbstractQuery;
import ifs.group08.task5.query.QueryParameter;

public class BuildingsQuery extends AbstractQuery {
	
	public BuildingsQuery() {
		super();

		this.parameters.put("limit", new QueryParameter("limit", "the number of buildings returned", false, true));
	}

	@Override
	public String getKey() {
		return "buildings";
	}

	@Override
	public String getDescription() {
		return "get a list of buildings, which have the most rooms";
	}

	@Override
	public boolean execute() {
		int limit = this.parameters.get("limit").getValue() == null ? 10 : Integer.parseInt(this.parameters.get("limit").getValue());

		String query = "PREFIX ifs: <http://ifs.tuwien.ac.at/tulid/group08/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
				"SELECT ?buildingCode ?name (COUNT(?room) as ?roomCount) " +
				"WHERE { " +
				"  ?building a ifs:Building; " +
				"	ifs:buildingCode ?buildingCode; " +
				"	foaf:name ?name. " +
				"  ?room a ifs:Room; " +
				"    ifs:roomBelongsTo ?building; " +
				"} " +
				"GROUP BY ?buildingCode ?name " +
				"ORDER BY DESC(?roomCount) " +
				"LIMIT " + limit;
		
		ResultSet results = this.sparqlService.executeQuery(query);
		
		System.out.println(limit + " largest buildings:\n");
		
		QuerySolution qs;
		int i = 1;
		while(results.hasNext()) {
			qs = results.next();
			System.out.println(
				String.format(
					"%s: %s room in building %s [%s]",
					i++,
					qs.getLiteral("roomCount").getInt(),
					qs.getLiteral("name"),
					qs.getLiteral("buildingCode")
				)
			);
		}
		
		return true;
	}

}
